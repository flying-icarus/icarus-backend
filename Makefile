.PHONY: install
install:
	pipenv install

install-dev:
	pipenv install --dev

.PHONY: api
api:
	pipenv run python -m icarus.api -c conf/local.icarus.yml --server

.PHONY: importer_opensky
importer_opensky:
	pipenv run python -m icarus.importers.opensky -c conf/local.icarus.yml

.PHONY: consumer
consumer:
	pipenv run python -m icarus.consumers.db_importer -c conf/local.icarus.yml

.PHONY: reset
reset:
	docker-compose kill
	docker-compose rm -vf
	docker-compose up -d
	sleep 30
	pipenv run python scripts/initdb.py \
	  --kafka-states-topic states \
	  --kafka-server 172.16.238.12 \
	  --cassandra-server 172.16.238.10 \
	  --s2cell-level 3 \
	  --bucket-size 30

.PHONY: protobuf
protobuf:
	protoc icarus.proto -I icarus/models --python_out=icarus/models

.PHONY: format
format:
	black icarus *.py

.PHONY: lint
lint:
	pipenv run black --check icarus *.py
	pipenv run mypy icarus

.PHONY: test
test:
	pipenv run python -m unittest discover

check-all: lint test
