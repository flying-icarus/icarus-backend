import logging
import functools

import falcon

import icarus.db
import icarus.utils
import icarus.models
import icarus.airportdata

log = logging.getLogger(__name__)

MIME_PBUF = "application/x-protobuf"
MIME_JSON = "application/json"


def negotiable(accepts, serializers):
    def _fn(responder):
        for mime_type in accepts:
            if mime_type not in serializers:
                raise Exception(f"Missing serializer for mime type {mime_type}.")

        @functools.wraps(responder)
        def __fn(self, req, resp, *args, **kwargs):
            preferred = req.client_prefers(accepts)
            if not preferred:
                raise falcon.HTTPNotAcceptable(acceptable=accepts)

            responder(self, req, resp, *args, **kwargs)

            serializer = serializers[preferred]
            serializer(self, resp)
            resp.content_type = preferred

        return __fn

    return _fn


class StatesResource:
    def __init__(self, db, secs_object_no_seen_is_active):
        self.db = db
        self._secs_object_no_seen_is_active = secs_object_no_seen_is_active

    def serialize_pbuf(self, resp):
        states_pbuf = icarus.models.icarus_pb2.ObjectStatesMap()
        for state in resp.states:
            state_pbuf = states_pbuf.states[state.icao]
            state_pbuf.MergeFrom(state.to_pbuf())

        resp.data = states_pbuf.SerializeToString()

    def serialize_json(self, resp):
        resp.media = {"states": {state.icao: state.astuple() for state in resp.states}}

    @negotiable(
        accepts=[MIME_PBUF, MIME_JSON],
        serializers={MIME_PBUF: serialize_pbuf, MIME_JSON: serialize_json},
    )
    def on_get(self, req, resp):
        timestamp = req.get_param_as_int("timestamp", required=True)
        bbox_str = req.get_param("bbox", required=True)
        bbox = [float(num) for num in bbox_str.split(",")]

        states = [
            state
            for state in self.db.get_states(timestamp, bbox)
            # return only objects alive within 2 minutes.
            if state.reported_at > (timestamp - self._secs_object_no_seen_is_active)
        ]

        resp.set_header("Access-Control-Allow-Origin", "*")
        # dataclass is not serializable.
        # TODO: inefficient.
        log.debug("api returns total states: %s", len(states))
        resp.states = states


class APIStatus:
    def on_get(self, req, resp):
        pass


class CurrentFlightResource:
    def __init__(self, db):
        self.db = db
        self.airportdata_client = icarus.airportdata.AirportDataClient()

    def serialize_pbuf(self, resp):
        flight_history_pbuf = icarus.models.icarus_pb2.FlightHistory()
        for state in resp.states:
            flight_history_pbuf.states.add().MergeFrom(state.to_pbuf())

        for picture in resp.pictures:
            flight_history_pbuf.pictures.add().MergeFrom(
                icarus.models.icarus_pb2.Picture(**picture.asdict())
            )

        resp.data = flight_history_pbuf.SerializeToString()

    def serialize_json(self, resp):
        resp.media = {"states": resp.states}

    @negotiable(
        accepts=[MIME_PBUF, MIME_JSON],
        serializers={MIME_PBUF: serialize_pbuf, MIME_JSON: serialize_json},
    )
    def on_get(self, reqp, resp, icao):
        states = [state for state in self.db.get_last_flight_by_icao(icao)]

        resp.set_header("Access-Control-Allow-Origin", "*")
        log.debug("api returns total states: %s", len(states))

        try:
            resp.pictures = self.airportdata_client.get_pictures(
                icao=icao, num_pictures=1
            )
        except icarus.airportdata.AircraftNotFound:
            resp.pictures = []
        except Exception:
            resp.pictures = []
            log.exception("Unhandled exception when fetching pictures for %s", icao)

        resp.states = states


class AircraftHistory:
    POSSIBLE_FILTERS = {
        "icao": {
            "db_method": "get_flights_by_icao",
            "get_param_method": "get_param_as_int",
        },
        "callsign": {
            "db_method": "get_flights_by_callsign",
            "get_param_method": "get_param",
        },
    }

    def __init__(self, db):
        self.db = db

    def on_get(self, req, resp):
        filter_name = None
        for param in self.POSSIBLE_FILTERS:
            if req.has_param(param):
                filter_name = param
                break
        else:
            raise falcon.HTTPBadRequest(
                description="Didn't find any query param filter"
            )

        filter_matched = self.POSSIBLE_FILTERS[filter_name]
        get_param_method = getattr(req, filter_matched["get_param_method"])

        filter_value = get_param_method(filter_name)
        db_method = getattr(self.db, filter_matched["db_method"])

        # TODO: add custom json serializer to support datetimes
        flights = [
            {
                "id": str(flight.id),
                "first_seen_at": flight.first_seen_at.isoformat(),
                "last_seen_at": flight.last_seen_at and flight.last_seen_at.isoformat(),
            }
            for flight in db_method(filter_value)
        ]

        resp.set_header("Access-Control-Allow-Origin", "*")
        resp.media = {"flights": flights}


def get_app(conf, uwsgi=True):
    api = falcon.API()
    db = icarus.db.DBClient(**conf["db"])

    if uwsgi:
        import uwsgidecorators

        @uwsgidecorators.postfork
        def _connect():
            db.connect()

    else:
        db.connect()

    api.add_route(
        "/states",
        StatesResource(
            db=db, secs_object_no_seen_is_active=conf["secs_object_no_seen_is_active"]
        ),
    )

    api.add_route("/active-flights/{icao:int}", CurrentFlightResource(db))
    api.add_route("/aircraft-history", AircraftHistory(db))
    api.add_route("/api-status", APIStatus())
    return api
