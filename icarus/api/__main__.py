import argparse

import icarus.api
import icarus.models
import icarus.config


parser = argparse.ArgumentParser()
parser.add_argument("-c", "--conf", default="/etc/icarus/icarus.yml")

parser.add_argument("-s", "--server", default=False, action="store_true")

args = parser.parse_args()
conf = icarus.config.load_config_file(args.conf)["api"]
icarus.config.configure_logging(**conf.pop("logging"))
application = icarus.api.get_app(conf, uwsgi=not args.server)

if args.server:
    import wsgiref.simple_server

    httpd = wsgiref.simple_server.make_server("127.0.0.1", 8000, application)
    httpd.serve_forever()
