from typing import ClassVar, Tuple, Optional
import uuid
import json
import dataclasses

import icarus.models.icarus_pb2
import icarus.utils
import icarus.exceptions


class BaseDataClass:
    FIELDS: ClassVar[Tuple]

    def asdict(self):
        return dataclasses.asdict(self)

    def astuple(self):
        return dataclasses.astuple(self)

    def items(self):
        for field in self.FIELDS:
            yield (field.name, getattr(self, field.name))


def dataclass_add_fields(cls):
    cls.FIELDS = dataclasses.fields(cls)
    return cls


@dataclass_add_fields
@dataclasses.dataclass
class ObjectState(BaseDataClass):
    VALUE_AVRO_SCHEMA = json.dumps(
        {
            "namespace": "icarus",
            "name": "object_state_value",
            "type": "record",
            "fields": [
                {"name": "icao", "type": "int"},
                {"name": "callsign", "type": "string"},
                {"name": "lat", "type": "float"},
                {"name": "lon", "type": "float"},
                {"name": "geo_altitude", "type": "float"},
                {"name": "baro_altitude", "type": "float"},
                {"name": "velocity", "type": "float"},
                {"name": "heading", "type": "float"},
                {"name": "vertical_rate", "type": "float"},
                {"name": "squawk", "type": "int"},
            ],
        }
    )

    KEY_AVRO_SCHEMA = json.dumps(
        {
            "namespace": "icarus",
            "name": "object_state_key",
            "type": "record",
            "fields": [{"name": "icao", "type": "int"}],
        }
    )

    icao: int
    reported_at: float
    lat: float
    lon: float
    callsign: Optional[str] = None
    geo_altitude: Optional[float] = None
    baro_altitude: Optional[float] = None
    velocity: Optional[float] = None
    heading: Optional[float] = None
    vertical_rate: Optional[float] = None
    squawk: Optional[int] = None

    def compress(self):
        reported_at = round(self.reported_at)
        if not (0 <= reported_at <= 0xFFFFFFFF):
            raise icarus.exceptions.ValueOutOfBounds(
                self.reported_at, "reported_at doesnt fit in (0 <= number <= FFFFFFFF)"
            )

        if self.velocity is not None:
            velocity = round(self.velocity)
            if not (0 <= velocity <= 0xFFFF):
                raise icarus.exceptions.ValueOutOfBounds(
                    self.velocity, "velocity doesnt fit in (0 <= number <= 0xFFFF)"
                )

            self.velocity = velocity

        if self.heading is not None:
            # We keep only two digits for the fractional part.
            heading = round(self.heading * 100)
            if not (0 <= heading <= 0xFFFF):
                raise icarus.exceptions.ValueOutOfBounds(
                    self.heading, "heading doesnt fit in (0 <= number <= 0xFFFF)"
                )

            self.heading = heading

        if self.baro_altitude is not None:
            # altitudes can be negative up to a small extend. Save up 1000ft
            # for negative altitudes.
            baro_altitude = round(self.baro_altitude) + 1000
            if not (0 <= baro_altitude <= 0xFFFF):
                raise icarus.exceptions.ValueOutOfBounds(
                    self.baro_altitude,
                    "baro_altitude doesnt fit in (0 <= number <= 0xFFFF)",
                )
            self.baro_altitude = baro_altitude

        if self.geo_altitude is not None:
            # altitudes can be negative up to a small extend. Save up 1000ft
            # for negative altitudes.
            geo_altitude = round(self.geo_altitude) + 1000
            if not (0 <= geo_altitude <= 0xFFFF):
                raise icarus.exceptions.ValueOutOfBounds(
                    self.geo_altitude,
                    "geo_altitude doesnt fit in (0 <= number <= 0xFFFF)",
                )
            self.geo_altitude = geo_altitude

        if self.vertical_rate is not None:
            # smallint signed range is "-32768 <= number <= 32767".
            if not (-32768 <= self.vertical_rate <= 32767):
                raise icarus.exceptions.ValueOutOfBounds(
                    self.vertical_rate,
                    "vertical_rate doesnt fit in (-32768 <= number <= 32767)",
                )

            self.vertical_rate = round(self.vertical_rate)

        if self.lat is not None:
            self.lat = icarus.utils.degree_to_int32(self.lat)

        if self.lon is not None:
            self.lon = icarus.utils.degree_to_int32(self.lon)

    def to_pbuf_lossy(self):
        try:
            # TODO: inefficient.
            return icarus.models.icarus_pb2.ObjectStateLossy(**self.asdict())
        except ValueError as exc:
            raise icarus.exceptions.ValueOutOfBounds(self.asdict(), str(exc))

    def to_pbuf(self):
        try:
            return icarus.models.icarus_pb2.ObjectState(**self.asdict())
        except ValueError as exc:
            raise icarus.exceptions.ValueOutOfBounds(self.asdict(), str(exc))

    @classmethod
    def from_pbuf_lossy(cls, buf, decompress=True):
        state_pbuf = icarus.models.icarus_pb2.ObjectStateLossy()
        state_pbuf.ParseFromString(buf)

        state_dict = {
            field.name: getattr(state_pbuf, field.name) for field in cls.FIELDS
        }

        # Even when the field is absent, there's no notion of null strings, so
        # it returns an empty string.
        if state_dict["callsign"] == "":
            state_dict["callsign"] = None

        state = cls(**state_dict)

        if decompress:
            if state.heading is not None:
                state.heading = state.heading / 100

            if state.baro_altitude is not None:
                state.baro_altitude = state.baro_altitude - 1000

            if state.geo_altitude is not None:
                state.geo_altitude = state.geo_altitude - 1000

            if state.lat is not None:
                state.lat = icarus.utils.int32_to_degree(state.lat)

            if state.lon is not None:
                state.lon = icarus.utils.int32_to_degree(state.lon)

        return state


@dataclasses.dataclass
class Flight(BaseDataClass):
    _id: uuid.UUID
    first_seen_at: float
    last_seen_at: float
    icao: int
    callsign: str


@dataclasses.dataclass
class ActiveFlight(BaseDataClass):
    icao: int
    flight_id: uuid.UUID
    first_seen_at: float
    last_seen_at: float
    callsign: str
