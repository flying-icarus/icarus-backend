class ValueOutOfBounds(Exception):
    def __init__(self, value, extra_msg=""):
        super().__init__(
            f"Value '{value}' is out of bounds for its designated field. {extra_msg}"
        )
