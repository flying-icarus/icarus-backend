from typing import Dict
import collections
import time

import icarus.store
import icarus.models
import icarus.db


# TODO: UNUSED.
class StateDeduplicator:
    def __init__(self):
        self._cache = {}
        now = time.time()
        self._bucket = icarus.db.timestamp_to_bucket(now)
        # ICAO is not cacheable
        self._object_state_fields = [
            field for field in icarus.models.ObjectState.FIELDS if field.name != "icao"
        ]

    def diff_state(self, new_state: icarus.models.ObjectState):
        # TODO: hack to fix typing
        bucket = icarus.db.timestamp_to_bucket(new_state.reported_at, None)
        if self._bucket != bucket:
            self._bucket = bucket
            self._cache = {}

        icao = new_state.icao
        if icao not in self._cache:
            self._cache[icao] = new_state
            return True

        old_state = self._cache[icao]

        # TODO: don't cheat dataclass.
        has_diff = False
        for field in self._object_state_fields:
            new_val = getattr(new_state, field.name)
            old_val = getattr(old_state, field.name)

            if new_val is None:
                continue

            if new_val == old_val:
                setattr(new_state, field.name, None)
                print(f"duplicate value {field.name}: {new_val}")
            else:
                setattr(old_state, field.name, new_val)
                has_diff = True

        return has_diff

    def get_state(self, icao):
        return self._cache[icao]

    def set_state(self, state):
        self._cache[state.icao] = state


class DeduplicatorMiddleware(icarus.store.StateStoreMiddleware):
    def __init__(self, cache_size: int, **kwargs):
        super().__init__(**kwargs)
        self._cache_size = cache_size
        self._cache: Dict[int, icarus.models.ObjectState] = collections.OrderedDict()

    def handle_state(self, store, state):
        try:
            old_state = self._cache[state.icao]
        except KeyError:
            if len(self._cache) == self._cache_size:
                self._cache.popitem(last=False)

            self._cache[state.icao] = state
            return state

        for field in state.FIELDS:
            # Don't dedup the key.
            if field.name == "icao":
                continue

            new_val = getattr(state, field.name)
            old_val = getattr(old_state, field.name)

            if new_val is None:
                continue

            if new_val == old_val:
                setattr(state, field.name, None)
                self._log.debug("Deduplicated value for %s", field.name)
            else:
                setattr(old_state, field.name, new_val)

        return state

    def clear(self):
        self._cache.clear()

    def clear_by_icao(self, icao: int):
        del self._cache[icao]
