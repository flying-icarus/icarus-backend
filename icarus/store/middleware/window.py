from typing import Dict, Any
import collections
import dataclasses
from datetime import timedelta

import icarus.models
import icarus.store


TWO_MINUTES = int(timedelta(minutes=2).total_seconds())


@dataclasses.dataclass
class _WindowedValue:
    value: Any
    updated_at: float


class WindowingPreservatorMiddleware(icarus.store.StateStoreMiddleware):
    _windowing_store: Dict[int, Dict[str, _WindowedValue]]

    def __init__(self, window_size=TWO_MINUTES, **kwargs):
        super().__init__(**kwargs)
        self._windowing_store = collections.defaultdict(dict)
        self._window_size = window_size

    def handle_state(
        self, store: icarus.store.StateStore, state: icarus.models.ObjectState
    ):
        for field in state.FIELDS:
            if field.name == "icao" or field.name == "reported_at":
                continue

            self.update_windowed_field(state, field.name)

        return state

    def update_windowed_field(self, state: icarus.models.ObjectState, field_name: str):
        assert field_name != "icao"
        value = getattr(state, field_name)
        if value is None:
            old_value = self.get_windowed_value(state=state, field_name=field_name)

            if old_value is not None:
                # Just a micro optimization. No point overwriting None with
                # None.
                setattr(state, field_name, old_value)
        else:
            self.set_windowed_value(value=value, state=state, field_name=field_name)

        return state

    def clear(self):
        self._windowing_store = collections.defaultdict(dict)

    def clear_by_icao(self, icao: int):
        del self._windowing_store[icao]

    def get_windowed_value(self, state: icarus.models.ObjectState, field_name: str):
        windowing_state = self._windowing_store[state.icao]
        if field_name in windowing_state:
            windowed_value = windowing_state[field_name]
            if (state.reported_at - windowed_value.updated_at) < self._window_size:
                value = windowed_value.value
                assert value is not None
                return value
            else:
                del windowing_state[field_name]

        return None

    def set_windowed_value(
        self, value: Any, state: icarus.models.ObjectState, field_name: str
    ):
        assert value is not None

        windowing_state = self._windowing_store[state.icao]
        if field_name not in windowing_state:
            windowing_state[field_name] = _WindowedValue(
                value=value, updated_at=state.reported_at
            )
        else:
            windowed_value = windowing_state[field_name]
            windowed_value.value = value
            windowed_value.updated_at = state.reported_at
