from __future__ import annotations

from typing import Dict, List, Set, Optional
import asyncio
import logging
import time
import datetime

import icarus.models
import icarus.utils


_TEN_MINUTES = datetime.timedelta(minutes=10).total_seconds()


class StateStoreMiddleware:
    def __init__(self, log: logging.Logger):
        self._log = log

    def handle_state(self, store: StateStore, state: icarus.models.ObjectState):
        raise NotImplementedError

    def clear(self):
        pass

    def clear_by_icao(self, icao: int):
        pass


class StateStore:
    _store: Dict[int, icarus.models.ObjectState]
    _middlewares: List[StateStoreMiddleware]
    _log: logging.Logger
    _remove_after_secs: Optional[int]

    def __init__(self, log: logging.Logger, remove_after_secs: Optional[int] = None):
        self._store = {}
        self._middlewares = []
        self._log = log
        self._remove_after_secs = remove_after_secs

    def add_middleware(self, middleware: StateStoreMiddleware):
        self._middlewares.append(middleware)

    def handle_state(self, state: icarus.models.ObjectState):
        for middleware in self._middlewares:
            state = middleware.handle_state(store=self, state=state)

        self._store[state.icao] = state

    def get_state(self, icao: int):
        return self._store[icao]

    def clear(self):
        self._store = {}
        for middleware in self._middlewares:
            middleware.clear()

    def clear_by_icao(self, icao: int):
        del self._store[icao]
        for middleware in self._middlewares:
            middleware.clear_by_icao(icao)

    def clear_old_states(self, now=None):
        self._log.info("Clearing old states.")
        assert self._remove_after_secs is not None
        if now is None:
            now = time.time()

        states_to_be_cleaned: Set[int] = set()
        prev_count = len(self._store)
        for state in self._store.values():
            if (now - state.reported_at) > self._remove_after_secs:
                states_to_be_cleaned.add(state.icao)

        for icao in states_to_be_cleaned:
            self.clear_by_icao(icao)

        count = len(states_to_be_cleaned)
        self._log.info(f"Cleared {count} out of {prev_count} states.")

    async def _worker_clear_old_states(self):
        while True:
            self.clear_old_states()
            await asyncio.sleep(_TEN_MINUTES)

    def start_workers(self):
        if self._remove_after_secs is not None:
            worker = icarus.utils.Worker(
                log=self._log, rerun_on_error=True, run=self._worker_clear_old_states
            )

            self._cleanup_store_task = worker.start()

    async def stop(self):
        if self._remove_after_secs is not None:
            self._cleanup_store_task.cancel()
            try:
                await self._cleanup_store_task
            except asyncio.CancelledError:
                pass
