from typing import List
import requests
import dataclasses


import icarus.models
import icarus.utils


class AircraftNotFound(Exception):
    pass


@dataclasses.dataclass
class PicturesResponsePicture(icarus.models.BaseDataClass):
    thumbnail_url: str
    picture_page_url: str
    photographer_fullname: str


class AirportDataClient:
    def get_pictures(self, icao, num_pictures) -> List[PicturesResponsePicture]:
        # http://www.airport-data.com/api/ac_thumb.json?m=400A0B&n=3
        http_resp = requests.get(
            "http://www.airport-data.com/api/ac_thumb.json",
            params={"m": icarus.utils.int_to_hex(icao), "n": str(num_pictures)},
        )

        if http_resp.status_code == 404:
            return []

        http_resp.raise_for_status()

        http_data = http_resp.json()
        if http_data["status"] != 200:
            raise AircraftNotFound(f"invalid response {http_data}")

        return [
            PicturesResponsePicture(
                thumbnail_url=http_picture["image"],
                picture_page_url=http_picture["link"],
                photographer_fullname=http_picture["photographer"],
            )
            for http_picture in http_data["data"]
        ]
