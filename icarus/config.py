import logging
import datetime
import yaml


def load_config_file(path):
    with open(path, "r") as f:
        yaml_config = yaml.safe_load(f)

    return yaml_config


def configure_logging(level: str):
    logging.basicConfig(
        level=getattr(logging, level.upper()),
        format="%(asctime)s %(levelname)s %(name)s: %(message)s",
    )


CASSANDRA_SERVERS = ["localhost"]

KAFKA_STATES_TOPIC = "states"
KAFKA_STATES_CONSUMERS_BATCH_SIZE = 1000
KAFKA_BOOTSTRAP_SERVERS = "localhost"

# bad naming for the amount of time an object will be shown on the live map
# even though we haven't seen it recently.
SECS_OBJECT_NO_SEEN_IS_ACTIVE = int(datetime.timedelta(minutes=2).total_seconds())

# amount of time a flight entry is kept open even though the object has not
# been seen recently.
SECS_FLIGHT_NO_SEEN_IS_ACTIVE = int(datetime.timedelta(minutes=50).total_seconds())

S2_CELL_LEVEL = 3

DB_BUCKET_SIZE = int(datetime.timedelta(seconds=30).total_seconds())
STATE_DEDUPLICATION = False
