import prometheus_async.aio.web
import prometheus_client


async def start_http_server(*, addr="", port=8080):
    await prometheus_async.aio.web.start_http_server(addr=addr, port=port)


# Expose Prometheus stuff.
Gauge = prometheus_client.Gauge
Histogram = prometheus_client.Histogram
