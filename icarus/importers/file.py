import json
import logging

import icarus.importers
import icarus.models

log = logging.getLogger(__name__)


def to_own_model(state):
    return icarus.models.ObjectState(
        icao=state["icao24"],
        callsign=state["callsign"],
        lat=state["latitude"],
        lon=state["longitude"],
        geo_altitude=state["geo_altitude"],
        baro_altitude=state["baro_altitude"],
        velocity=state["velocity"],
        heading=state["heading"],
        vertical_rate=state["vertical_rate"],
        squawk=state["squawk"],
    )


class FileImporter(icarus.importers.BaseImporter):
    def __init__(self, file_path):
        super().__init__()
        self.file_path = file_path

    def run(self):
        with open(self.file_path) as f:
            for line in f:
                state = to_own_model(json.loads(line))
                self.import_state(state)


if __name__ == "__main__":
    importer = FileImporter("data/2019-01-22T21.dat")
    importer.run()
