import asyncio
import argparse

import icarus.config
import icarus.importers.opensky


parser = argparse.ArgumentParser()
parser.add_argument("-c", "--conf", default="/etc/icarus/icarus.yml")

args = parser.parse_args()

conf = icarus.config.load_config_file(args.conf)["importer_opensky"]
icarus.config.configure_logging(**conf.pop("logging"))
importer = icarus.importers.opensky.OpenSkyImporter(**conf)

asyncio.run(importer.run())
