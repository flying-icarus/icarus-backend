import functools
import logging
import asyncio
import datetime

import icarus.importers
import icarus.models
import icarus.metrics

import opensky_api

log = logging.getLogger(__name__)


def mps_to_knots(mps):
    """Meters per second to Knots."""
    return mps * 3600 / 1852


def m_to_ft(m):
    return m / 0.3048


def mps_to_ftm(mps):
    return m_to_ft(mps) * 60


def to_own_model(state):
    def apply_if_not_none(fn, value):
        if value is not None:
            return fn(value)

    # callsign is fixed length to 8 chars so if the actual characters are
    # less, it is suffixed by spaces.
    callsign = state.callsign.strip()
    if not callsign:
        callsign = None

    return icarus.models.ObjectState(
        icao=int(state.icao24, 16),
        callsign=callsign,
        lat=state.latitude,
        lon=state.longitude,
        geo_altitude=apply_if_not_none(m_to_ft, state.geo_altitude),
        baro_altitude=apply_if_not_none(m_to_ft, state.baro_altitude),
        velocity=apply_if_not_none(mps_to_knots, state.velocity),
        heading=state.heading,
        vertical_rate=apply_if_not_none(mps_to_ftm, state.vertical_rate),
        squawk=int(state.squawk) if state.squawk is not None else None,
        reported_at=state.last_contact,
    )


class OpenSkyImporter(icarus.importers.BaseImporter):
    def __init__(
        self, opensky_username, opensky_password, kafka_servers, kafka_states_topic
    ):
        super().__init__(
            kafka_servers=kafka_servers,
            kafka_states_topic=kafka_states_topic,
            log=log,
            store_remove_after_secs=datetime.timedelta(minutes=30).total_seconds(),
        )

        self.opensky = opensky_api.OpenSkyApi(
            username=opensky_username, password=opensky_password
        )

        # Metrics.
        self.metric_num_imported_states = icarus.metrics.Histogram(
            "num_imported_states", "Number of imported states."
        )

    async def run(self):
        await super().run()
        self.start_workers()
        while True:
            await self.run_once()
            log.debug("Sleeping")
            await asyncio.sleep(15)

    async def run_once(self):
        log.debug("Getting states")
        loop = asyncio.get_event_loop()
        opensky_states = (
            await loop.run_in_executor(
                None, functools.partial(self.opensky.get_states, timeout=30)
            )
        ).states

        states = [
            # TODO: we're missing on things here.
            to_own_model(state)
            for state in opensky_states
            if state.latitude and state.longitude
        ]

        log.info("Importing %s states", len(states))
        self.metric_num_imported_states.observe(len(states))

        for state in states:
            self.import_state(state)
