import asyncio
from typing import List, Optional
import logging
import struct

import confluent_kafka as kafka

import icarus.store
import icarus.store.middleware
import icarus.models
import icarus.exceptions
import icarus.metrics


class BaseImporter:
    log: logging.Logger
    producer: kafka.Producer
    _store: icarus.store.StateStore
    _kafka_states_topic: str

    def __init__(
        self,
        kafka_servers: List[str],
        kafka_states_topic: str,
        log: logging.Logger,
        store_remove_after_secs: Optional[int] = None,
    ):
        self.log = log
        self._kafka_states_topic = kafka_states_topic
        self.producer = kafka.Producer({"bootstrap.servers": ",".join(kafka_servers)})
        self._store = icarus.store.StateStore(
            log=log, remove_after_secs=store_remove_after_secs
        )

        self._store.add_middleware(
            icarus.store.middleware.WindowingPreservatorMiddleware(log=self.log)
        )

    async def run(self):
        await icarus.metrics.start_http_server()

    def import_state(self, state: icarus.models.ObjectState):
        try:
            state.compress()
        except icarus.exceptions.ValueOutOfBounds as exc:
            self.log.info("Skipping out of bounds state: %s.", exc)
            return

        try:
            prev_state = self._store.get_state(state.icao)
        except KeyError:
            self._store.handle_state(state)
        else:
            # Skip completely duplicate entries.
            if state == prev_state:
                self.log.debug("Skipping duplicate entry.")
                return

        state_pbuf = state.to_pbuf_lossy()

        self.producer.produce(
            self._kafka_states_topic,
            key=struct.pack("<I", state.icao),
            value=state_pbuf.SerializeToString(),
            callback=self._delivery_callback,
        )

        self.producer.flush()

    def _delivery_callback(self, err, msg):
        if err:
            raise err

    def start_workers(self):
        self._store.start_workers()

    async def stop(self):
        await self._store.stop()
