import logging
import unittest
import functools

import icarus.store.middleware
import icarus.models


log = logging.getLogger()


class TestWindowingPreservatorMiddleware(unittest.TestCase):
    WINDOW_SIZE = 10

    def testWindow(self):
        icao = 1
        reported_at = 2
        middleware = icarus.store.middleware.WindowingPreservatorMiddleware(
            log=log, window_size=self.WINDOW_SIZE
        )

        create_state = functools.partial(
            icarus.models.ObjectState, icao=icao, lat=10, lon=11
        )

        # Start with an empty callsign.
        state = middleware.handle_state(
            store=None, state=create_state(reported_at=reported_at, callsign=None)
        )

        self.assertEqual(state.icao, 1)
        self.assertEqual(state.callsign, None)

        # Set it.
        state = middleware.handle_state(
            store=None, state=create_state(reported_at=reported_at, callsign="callsign")
        )

        self.assertEqual(state.icao, 1)
        self.assertEqual(state.callsign, "callsign")

        # In the same window, unset it.
        reported_at += self.WINDOW_SIZE - 1
        state = middleware.handle_state(
            store=None, state=create_state(reported_at=reported_at, callsign=None)
        )

        # Confirm the value is preserved.
        self.assertEqual(state.icao, 1)
        self.assertEqual(state.callsign, "callsign")

        # In the next window, unset it.
        reported_at += self.WINDOW_SIZE
        state = middleware.handle_state(
            store=None, state=create_state(reported_at=reported_at, callsign=None)
        )

        # Confirm the value is not preserved.
        self.assertEqual(state.icao, 1)
        self.assertEqual(state.callsign, None)
