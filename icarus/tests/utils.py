import random
import string


def random_str(length):
    """Returns a random string of the required length."""
    candidates = string.digits + string.ascii_letters
    return "".join(random.choice(candidates) for _ in range(length)).lower()
