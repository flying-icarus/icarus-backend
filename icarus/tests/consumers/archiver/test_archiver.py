import unittest.mock

import math

import icarus.tests
import icarus.consumers.archiver


class ArchiverTestCase(icarus.tests.TestCase):
    def setUp(self):
        super().setUp()
        with unittest.mock.patch("b2blaze.B2", autospec=True) as b2_mock:
            self.b2_mock = b2_mock
            self.archiver = icarus.consumers.archiver.ObjectStateArchiver(
                b2_app_key_id="b2_app_key_id",
                b2_app_key_secret="b2_app_key_secret",
                b2_bucket_name="b2_bucket_name",
                deduplicator_cache_size=10000,
                # 1G,
                lzma_max_dict_size=int(math.pow(1024, 3)),
                kafka_servers=[str(self.testbed.kafka_ip)],
                kafka_states_topic="states",
                kafka_consume_batch_size=1,
            )

    def testArchiver(self):
        pass
