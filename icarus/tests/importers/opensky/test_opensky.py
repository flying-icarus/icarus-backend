import asyncio
import icarus.tests
import icarus.importers.opensky


class OpenskyTestCase(icarus.tests.TestCase):
    @icarus.tests.async_test
    async def testOpensky(self):
        importer = icarus.importers.opensky.OpenSkyImporter(
            opensky_username=None,
            opensky_password=None,
            kafka_servers=[str(self.testbed.kafka_ip)],
            kafka_states_topic=self.testbed.STATES_TOPIC_NAME,
        )

        importer.start()
        importer.run_once()
        await asyncio.sleep(2)
        await importer.stop()
