from typing import Dict, List, Any
import asyncio
import time
import json
import logging
import unittest
import subprocess
import ipaddress

import confluent_kafka.admin as kafka_admin

import icarus.tests.utils


log = logging.getLogger()


class IcarusTestbed:
    STATES_TOPIC_NAME = "states"

    def __init__(self):
        self._docker_network_id = "icarus-testbed-{}".format(
            icarus.tests.utils.random_str(4)
        )

        self._zookeeper_container_name = self._docker_network_id + "-zookeeper"
        self._kafka_container_name = self._docker_network_id + "-kafka"
        self._ip_network = None

    def start(self):
        self._create_docker_network()
        self._start_zookeeper()
        self._start_kafka()

    def _create_docker_network(self):
        log.info("Creating docker network %s", self._docker_network_id)
        # Find an overlay network that is not in use
        subprocess.check_output(
            ["docker", "network", "create", self._docker_network_id],
            stderr=subprocess.STDOUT,
        )

    def _docker_run(self, env: Dict[str, str], name: str, image: str):
        env_args: List[str] = []
        for k, v in env.items():
            env_args.extend(["--env", f"{k}={v}"])

        subprocess_args = [
            "docker",
            "run",
            "--rm",
            "--name",
            name,
            "--network",
            self._docker_network_id,
        ]

        subprocess_args.extend(env_args)
        subprocess_args.append(image)
        proc = subprocess.Popen(
            subprocess_args,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
            stdin=subprocess.DEVNULL,
        )
        time.sleep(2)
        return proc

    def _get_docker_container_ip(self, name: str) -> ipaddress.IPv4Address:
        inspect_networks: str = subprocess.check_output(
            [
                "docker",
                "container",
                "inspect",
                name,
                "--format",
                "{{json .NetworkSettings.Networks}}",
            ]
        )

        networks: Dict[str, Any] = json.loads(inspect_networks)
        our_network: Dict[str, Any] = networks[self._docker_network_id]
        return ipaddress.ip_address(our_network["IPAddress"])

    def _start_zookeeper(self):
        self._zookeeper_docker_container = self._docker_run(
            env={"ALLOW_ANONYMOUS_LOGIN": "yes"},
            name=self._zookeeper_container_name,
            image="bitnami/zookeeper:latest",
        )

    def _start_kafka(self):
        zookeeper_ip = self._get_docker_container_ip(self._zookeeper_container_name)

        # TODO: cheat
        kafka_ip = zookeeper_ip + 1
        self._kafka_docker_container = self._docker_run(
            env={
                "KAFKA_CFG_ZOOKEEPER_CONNECT": f"{zookeeper_ip}:2181",
                "ALLOW_PLAINTEXT_LISTENER": "yes",
                "KAFKA_ADVERTISED_LISTENERS": f"PLAINTEXT://{kafka_ip}:9092",
            },
            name=self._kafka_container_name,
            image="bitnami/kafka:latest",
        )

        self.kafka_ip = self._get_docker_container_ip(self._kafka_container_name)
        assert kafka_ip == self.kafka_ip
        kafka_client = kafka_admin.AdminClient(
            {
                "bootstrap.servers": self.kafka_ip,
                "group.id": "mygroup",
                "auto.offset.reset": "earliest",
            }
        )

        futures = kafka_client.create_topics(
            [
                kafka_admin.NewTopic(
                    topic=IcarusTestbed.STATES_TOPIC_NAME,
                    num_partitions=1,
                    replication_factor=1,
                    config={
                        # TODO: switch to zstd when support lands in librdkafka.
                        "compression.type": "snappy"
                    },
                )
            ]
        )

        for fut in futures.values():
            fut.result()

    def stop(self):
        for (action_desc, fn) in [
            ("stop docker containers", self._stop_docker_containers),
            ("remove docker network", self._remove_docker_network),
        ]:
            try:
                fn()
            except Exception:
                log.exception("Failed to %s", action_desc)

    def _stop_docker_containers(self):
        network_metadata = subprocess.check_output(
            ["docker", "network", "inspect", self._docker_network_id]
        )

        network_metadata = json.loads(network_metadata)
        for network in network_metadata:
            for container in network["Containers"].values():
                container_id = container["Name"]
                log.info("Killing container %s", container_id)
                subprocess.check_output(["docker", "rm", "--force", container_id])

    def _remove_docker_network(self):
        log.info("Removing docker network %s", self._docker_network_id)
        subprocess.check_output(
            ["docker", "network", "remove", self._docker_network_id]
        )


class TestCase(unittest.TestCase):
    def setUp(self):
        self.testbed = IcarusTestbed()
        try:
            self.testbed.start()
        except BaseException:
            # Catch KeyboardInterrupt too.
            # If setUp() raises, it won't run tearDown(), so we need to clean
            # up manually.
            self.testbed.stop()
            raise

    def tearDown(self):
        self.testbed.stop()


# https://stackoverflow.com/questions/23033939/how-to-test-python-3-4-asyncio-code
def async_test(coro):
    def wrapper(*args, **kwargs):
        loop = asyncio.new_event_loop()
        return loop.run_until_complete(coro(*args, **kwargs))

    return wrapper
