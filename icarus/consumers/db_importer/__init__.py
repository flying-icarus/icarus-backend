import uuid
import logging
import time

import icarus
import icarus.db
import icarus.consumers
import icarus.exceptions
import icarus.models


log = logging.getLogger(__name__)


class ObjectStateConsumer(icarus.consumers.BaseConsumer):
    def __init__(
        self,
        active_flights_cleanup_interval,
        commit_every_messages,
        messages_consume_per_iteration,
        bucket_size,
        s2cell_level,
        cassandra_servers,
        kafka_servers,
        kafka_states_topic,
        kafka_consume_batch_size,
        secs_flight_not_seen_is_active,
    ):
        super().__init__(
            group_id="states_to_db",
            topic=kafka_states_topic,
            kafka_servers=kafka_servers,
            batch_size=kafka_consume_batch_size,
        )

        self._secs_flight_not_seen_is_active = secs_flight_not_seen_is_active
        self.active_flights_cleanup_interval = active_flights_cleanup_interval

        # Prevent cleanups until we deplete the messages.
        self._last_active_flights_cleanup = float("inf")

        self.commit_every_messages = commit_every_messages
        self._uncommitted_msgs = 0
        self._batch_futures = []
        self.db = icarus.db.DBClient(
            bucket_size=bucket_size,
            s2cell_level=s2cell_level,
            cassandra_servers=cassandra_servers,
        )
        self.db.connect()

        log.info("Fetching active flights.")
        self.active_flights = self.db.get_active_flights()

    def _process_batch(self, msgs):
        for msg in msgs:
            futures = self._process_msg(msg)
            self._batch_futures.extend(futures)
            self._uncommitted_msgs += 1

        for future in self._batch_futures:
            future.result()

        self._batch_futures.clear()

        if self._uncommitted_msgs > self.commit_every_messages:
            self.consumer.commit()
            self._uncommitted_msgs = 0

        # TODO: do this in its own worker.
        now = time.time()
        if self._last_active_flights_cleanup < (
            now - self.active_flights_cleanup_interval
        ):
            self._cleanup_active_flights()
            self._last_active_flights_cleanup = time.time()

    def _new_flight_seen(self, flight_id, state):
        assert state.icao not in self.active_flights
        active_flight = icarus.models.ActiveFlight(
            icao=state.icao,
            flight_id=flight_id,
            first_seen_at=state.reported_at,
            last_seen_at=state.reported_at,
            callsign=state.callsign,
        )
        self.active_flights[state.icao] = active_flight
        fut_1 = self.db.add_flight(flight_id, state, asynchronous=True)
        fut_2 = self.db.set_active_flight(active_flight, asynchronous=True)

        return [fut_1, fut_2]

    def _update_active_flight_from_state(
        self, active_flight, state, update_callsign=True
    ):
        active_flight.last_seen_at = state.reported_at
        if update_callsign:
            active_flight.callsign = state.callsign
        return self.db.set_active_flight(active_flight, asynchronous=True)

    def _process_msg(self, msg):
        state = icarus.models.ObjectState.from_pbuf_lossy(msg.value())
        icao = state.icao
        futures = []

        if icao not in self.active_flights:
            flight_id = uuid.uuid4()
            futures = self._new_flight_seen(flight_id=flight_id, state=state)
            active_flight = self.active_flights[state.icao]
            log.info("Seen new flight %s.", active_flight)
            futures.extend(futures)
        elif self.active_flights[icao].callsign != state.callsign:
            active_flight = self.active_flights[icao]
            if active_flight.callsign is None and state.callsign is not None:
                log.info(
                    "Flight %s had no callsign but it's now set to %s.",
                    active_flight,
                    state.callsign,
                )

                fut = self._update_active_flight_from_state(active_flight, state)
                futures.append(fut)
            else:
                log.info(
                    "Resetting flight %s because it changed callsign (%s --> %s).",
                    active_flight,
                    active_flight.callsign,
                    state.callsign,
                )

                self._end_flight(active_flight)
                self._new_flight_seen(flight_id=uuid.uuid4(), state=state)
        else:
            active_flight = self.active_flights[icao]
            fut = self._update_active_flight_from_state(active_flight, state)
            futures.append(fut)

        try:
            fut = self.db.add_state(state, asynchronous=True)
            futures.append(fut)
        except icarus.exceptions.ValueOutOfBounds:
            log.exception("Error when adding state %r", state)

        return futures

    def _cleanup_active_flights(self):
        # Purposefully taking a copy of the keys because we're editing the
        # dictionary while iterating it.
        log.info("Cleaning up active flights.")
        now = time.time()
        futures = []
        for icao in list(self.active_flights.keys()):
            active_flight = self.active_flights[icao]
            if active_flight.last_seen_at < (
                now - self._secs_flight_not_seen_is_active
            ):
                log.info("Ending flight %s due to inactivity.", active_flight.flight_id)
                end_flight_futures = self._end_flight(active_flight=active_flight)

                futures.extend(end_flight_futures)

        for future in futures:
            future.result()

    def _end_flight(self, active_flight):
        fut1 = self.db.end_flight(
            flight_id=active_flight.flight_id,
            first_seen_at=active_flight.first_seen_at,
            last_seen_at=active_flight.last_seen_at,
            asynchronous=True,
        )

        fut2 = self.db.delete_active_flight(active_flight.icao, asynchronous=True)
        del self.active_flights[active_flight.icao]
        return [fut1, fut2]

    def _on_depletion(self):
        log.info("Depleted pending messages.")
        self._cleanup_active_flights()
