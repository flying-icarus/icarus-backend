import asyncio
import argparse
import signal
import logging

import icarus.config
import icarus.consumers.db_importer

log = logging.getLogger(__name__)
parser = argparse.ArgumentParser()
parser.add_argument("-c", "--conf", default="/etc/icarus/icarus.yml")

args = parser.parse_args()

conf = icarus.config.load_config_file(args.conf)["consumer_db_importer"]
icarus.config.configure_logging(**conf.pop("logging"))
consumer = icarus.consumers.db_importer.ObjectStateConsumer(**conf)


def _stop(sig_num, frame):
    log.error(f"Received signal {signal.Signals(sig_num).name}.")
    consumer.stop()


signal.signal(signal.SIGTERM, _stop)
asyncio.run(consumer.run())
