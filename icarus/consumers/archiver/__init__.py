import datetime
import io
import logging
import lzma

import icarus.models
import icarus.store
import icarus.store.middleware
import icarus.archive.b2

log = logging.getLogger(__name__)


class ObjectStateArchiver(icarus.consumers.BaseConsumer):
    _states_store: icarus.store.StateStore
    _curr_hour: int
    _deduplicator: icarus.store.middleware.DeduplicatorMiddleware

    def __init__(
        self,
        b2_app_key_id,
        b2_app_key_secret,
        b2_bucket_name,
        deduplicator_cache_size,
        lzma_max_dict_size,
        kafka_servers,
        kafka_states_topic,
        kafka_consume_batch_size,
    ):
        super().__init__(
            group_id="states_to_archive",
            topic=kafka_states_topic,
            kafka_servers=kafka_servers,
            batch_size=kafka_consume_batch_size,
        )

        self._uncommitted_msgs = 0
        self._lzma_max_dict_size = lzma_max_dict_size
        self._b2 = icarus.archive.b2.B2ArchiveStorage(
            b2_app_key_id=b2_app_key_id,
            b2_app_key_secret=b2_app_key_secret,
            b2_bucket_name=b2_bucket_name,
        )

        self._kafka_consume_batch_size = kafka_consume_batch_size
        self._curr_hour = None
        self._states_pbuf = icarus.models.icarus_pb2.ObjectStatesLossy()
        self._states_store = icarus.store.StateStore(log=log)
        self._deduplicator = icarus.store.middleware.DeduplicatorMiddleware(
            cache_size=deduplicator_cache_size, log=log
        )

        self._states_store.add_middleware(self._deduplicator)

    def _flush(self):
        # TODO: inefficiency at its finest.
        log.info("Flushing states to archive.")
        states_pbuf_bytes = self._states_pbuf.SerializeToString()
        compressor = lzma.LZMACompressor(
            filters=[
                {
                    "id": lzma.FILTER_LZMA2,
                    "dict_size": max(len(states_pbuf_bytes), self._lzma_max_dict_size),
                    "mf": lzma.MF_BT4,
                    "depth": 1000,
                    "mode": lzma.MODE_NORMAL,
                    "nice_len": 64,
                }
            ]
        )

        chunks = [compressor.compress(states_pbuf_bytes)]
        chunks.append(compressor.flush())
        date = datetime.datetime.utcfromtimestamp(self._curr_hour * 3600)
        file_name = date.strftime("%Y/%m/%d/%H.v1.dat.xz")
        log.info("Uploading to %s", file_name)
        contents = b"".join(chunks)
        self._b2.upload(file_name=file_name, stream=io.BytesIO(contents))

        self.consumer.commit()
        self._states_store.clear()
        self._states_pbuf.Clear()

    def _process_batch(self, msgs):
        for msg in msgs:
            state = icarus.models.ObjectState.from_pbuf_lossy(
                msg.value(), decompress=False
            )

            # This is not really the point in time the state vector was
            # captured, but when the message was added to the queue.
            # Unfortunately, state.reported_at will be delivered out of order,
            # because it is inserted out of order, so it will break our hourly
            # buckets.
            msg_hour = self.get_msg_timestamp(msg) // 3600

            # initialize the current hour based on the timestamp of the
            # first message we encounter.
            if self._curr_hour is None:
                self._curr_hour = msg_hour

            if msg_hour != self._curr_hour:
                log.info(
                    "Hour changed. Currently at %s but encountered message at %s",
                    self._curr_hour,
                    msg_hour,
                )

                # Make sure we're not being fed out of order states.
                assert msg_hour >= self._curr_hour

                self._flush()
                self._curr_hour = msg_hour

            self._states_store.handle_state(state)

            state_pbuf_child = self._states_pbuf.states.add()
            state_pbuf_child.MergeFrom(state.to_pbuf_lossy())
