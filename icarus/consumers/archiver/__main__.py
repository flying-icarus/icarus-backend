import argparse
import asyncio
import logging
import signal

import icarus.consumers.archiver
import icarus.config


log = logging.getLogger(__name__)
parser = argparse.ArgumentParser()
parser.add_argument("-c", "--conf", default="/etc/icarus/icarus.yml")

args = parser.parse_args()

conf = icarus.config.load_config_file(args.conf)["consumer_archiver"]
icarus.config.configure_logging(**conf.pop("logging"))
consumer = icarus.consumers.archiver.ObjectStateArchiver(**conf)


def _stop(sig_num, frame):
    log.error(f"Received signal {signal.Signals(sig_num).name}.")
    consumer.stop()


signal.signal(signal.SIGTERM, _stop)
asyncio.run(consumer.run())
