import asyncio
import logging
import time

import confluent_kafka as kafka


log = logging.getLogger(__name__)


class BaseConsumer:
    # If we let consume wait more than max.poll.interval.ms then it will leave
    # the consumer group.
    # TODO: reevaluate when this is fixed
    # https://github.com/confluentinc/confluent-kafka-python/issues/618
    MAX_POLL_INTERVAL_MS = 300000
    POLL_TIMEOUT = 0.9 * MAX_POLL_INTERVAL_MS / 1000

    DEPLETION_THRESHOLD = 30

    def __init__(self, *, group_id, batch_size, kafka_servers, topic):
        self._stop_event = None
        self._run_task = None
        self._has_depleted = False
        self.messages_consume_per_iteration = batch_size
        self.consumer = kafka.Consumer(
            {
                "bootstrap.servers": ",".join(kafka_servers),
                "group.id": group_id,
                "auto.offset.reset": "earliest",
                "enable.auto.commit": "false",
                "max.poll.interval.ms": BaseConsumer.MAX_POLL_INTERVAL_MS,
            }
        )

        self.consumer.subscribe(
            [topic],
            on_assign=self._on_subscription_assigned,
            on_revoke=self._on_subscription_revoked,
        )

    def _on_subscription_assigned(self, consumer, partitions):
        log.info("Got assigned partitions %s.", partitions)

    def _on_subscription_revoked(self, consumer, partitions):
        # We should possibly error out here?
        log.info("Got our subscription to partitions %s revoked.", partitions)

    def _on_depletion(self):
        """Called when the consumer has kept up with the latest messages."""
        pass

    async def run(self):
        log.debug("Starting run.")
        # It's important to initialize this inside the asyncio.run() code path,
        # because it creates a new event loop. Otherwise we'd have events and
        # tasks in different loops.
        self._stop_event = asyncio.Event()
        while not self._stop_event.is_set():
            msgs = self._fetch_batch()
            if not msgs:
                log.info("Timeout waiting for messages.")
                continue

            for msg in msgs:
                msg_error = msg.error()
                if msg_error:
                    raise Exception("Consumer error: {}".format(msg_error))

            if not self._has_depleted:
                now = time.time()
                for msg in msgs:
                    ts = self.get_msg_timestamp(msg)
                    if ts > (now - BaseConsumer.DEPLETION_THRESHOLD):
                        self._has_depleted = True
                        self._on_depletion()
                        break

            log.info("Processing batch.")
            self._process_batch(msgs)

        log.info("Exited consume loop.")
        log.info("Closing consumer.")
        self.consumer.close()

    def _fetch_batch(self):
        log.info("Fetching batch of %s.", self.messages_consume_per_iteration)
        return self.consumer.consume(
            num_messages=self.messages_consume_per_iteration,
            timeout=BaseConsumer.POLL_TIMEOUT,
        )

    def _process_batch(self):
        raise NotImplementedError

    @staticmethod
    def get_msg_timestamp(msg):
        ts_type, ts = msg.timestamp()
        if ts_type == kafka.TIMESTAMP_NOT_AVAILABLE:
            raise Exception("message missing timestamp information.")

        return ts / 1000

    def stop(self):
        log.info("Setting stop event.")
        self._stop_event.set()
