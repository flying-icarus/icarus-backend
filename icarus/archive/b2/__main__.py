import os.path
import shutil

import click

import icarus.archive.b2


@click.group()
def cli():
    pass


@cli.command()
@click.option(
    "--year", help="Restrict to files from that year.", type=click.INT, required=True
)
@click.option(
    "--month", help="Restrict to files from that month.", type=click.INT, required=True
)
@click.option(
    "--day", help="Restrict to files from that day.", type=click.INT, required=True
)
@click.option("--key-id", help="B2 app key ID.", required=True)
@click.option("--key-secret", help="B2 app key secret.", required=True)
@click.option("--bucket", help="Bucket name", required=True)
@click.option(
    "--destination", help="Directory where the files will be placed", required=True
)
def download(year, month, day, key_id, key_secret, bucket, destination):
    b2 = icarus.archive.b2.B2ArchiveStorage(
        b2_app_key_id=key_id, b2_app_key_secret=key_secret, b2_bucket_name=bucket
    )

    for file_name in b2.list_files(prefix=f"{year}/{month:02d}/{day:02d}"):
        print(f"Processing {file_name}")
        dst_dir_path = os.path.join(destination, os.path.dirname(file_name))
        dst_file_path = os.path.join(destination, file_name)
        os.makedirs(dst_dir_path, exist_ok=True)
        data = b2.get(file_name)
        with open(dst_file_path, "wb") as f:
            shutil.copyfileobj(data, f)


cli()
