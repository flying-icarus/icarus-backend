import icarus.archive
import b2blaze


class B2ArchiveStorage(icarus.archive.ArchiveStorage):
    def __init__(self, b2_app_key_id, b2_app_key_secret, b2_bucket_name):
        b2 = b2blaze.B2(key_id=b2_app_key_id, application_key=b2_app_key_secret)
        self._b2_bucket = b2.buckets.get(b2_bucket_name)
        if self._b2_bucket is None:
            raise Exception("Could not find bucket")

    def upload(self, file_name, stream):
        contents = stream.read()
        return self._b2_bucket.files.upload(file_name=file_name, contents=contents)

    def list_files(self, prefix):
        for _file in self._b2_bucket.files.all(prefix=prefix):
            yield _file.file_name

    def get(self, file_name):
        return self._b2_bucket.files.get(file_name=file_name).download()
