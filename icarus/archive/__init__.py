class ArchiveStorage:
    def upload(self, path, stream):
        raise NotImplementedError()

    def get(self, path):
        raise NotImplementedError()

    def list_files(self, prefix):
        raise NotImplementedError()
