from typing import Callable
import asyncio
import logging
import time
import contextlib
import struct

import icarus.exceptions


@contextlib.contextmanager
def measure_time(msg):
    before = time.perf_counter()
    yield
    after = time.perf_counter()
    print(f"{msg}: {after - before}")


def wrap_struct_error(fn):
    def _struct_operation(num):
        try:
            return fn(num)
        except struct.error:
            raise icarus.exceptions.ValueOutOfBounds(num, str(struct.error))

    return _struct_operation


@wrap_struct_error
def unsigned_to_signed_16(num):
    # TODO: probably there's an easier way to do this, but I'm a noob.
    return struct.unpack("@h", struct.pack("@H", num))[0]


@wrap_struct_error
def signed_to_unsigned_16(num):
    return struct.unpack("@H", struct.pack("@h", num))[0]


@wrap_struct_error
def unsigned_to_signed_32(num):
    return struct.unpack("@i", struct.pack("@I", num))[0]


@wrap_struct_error
def signed_to_unsigned_32(num):
    return struct.unpack("@I", struct.pack("@i", num))[0]


@wrap_struct_error
def degree_to_int32(degree):
    int32 = round(degree * 10000000)

    # two's complement range of a 32 bit signed integer.
    if not (-0x80000000 <= int32 < 0x7FFFFFFF):
        raise icarus.exceptions.ValueOutOfBounds(degree)

    return int32


def int32_to_degree(int32):
    return int32 / 10000000


def hex_to_int(h):
    return int(h, 16)


def int_to_hex(i):
    return hex(i)[2:]


class Worker:
    rerun_on_error: bool
    rerun_sleep_secs: int
    log: logging.Logger
    _stop_event: asyncio.Event

    def __init__(
        self,
        run: Callable,
        log: logging.Logger,
        rerun_on_error=False,
        rerun_sleep_secs=1,
    ):
        self._rerun_on_error = rerun_on_error
        self._rerun_sleep_secs = rerun_sleep_secs
        self._log = log
        self._run = run

    def start(self):
        return asyncio.create_task(self._run_task())

    async def _run_task(self):
        while True:
            try:
                await self._run()
                break
            except Exception as exc:
                if isinstance(exc, asyncio.CancelledError):
                    break

                if not self._rerun_on_error:
                    raise

                self._log.exception(
                    "Worker %s raised an exception. Rerunning it.", self
                )

                await asyncio.sleep(self._rerun_sleep_secs)

    async def stop(self):
        print("STOPPING")
        self._stop_event.set()
        await self._future
