import time
import itertools
import logging

import cassandra.cluster
import cassandra.policies
import cassandra.util
import cassandra.query
import s2sphere

import icarus.utils
import icarus.models
import icarus.utils

log = logging.getLogger(__name__)


def timestamp_to_bucket(timestamp, bucket_size):
    return (timestamp // bucket_size) * bucket_size


def _compute_all_s2cells(level):
    region_coverer = s2sphere.RegionCoverer()
    region_coverer.min_level = level
    region_coverer.max_level = level
    bbox = s2sphere.LatLngRect(
        s2sphere.LatLng.from_degrees(-180, -90), s2sphere.LatLng.from_degrees(180, 90)
    )

    s2cells = region_coverer.get_covering(bbox)
    return [s2cell.id() for s2cell in s2cells]


class DBClient:
    def __init__(
        self, bucket_size, s2cell_level, cassandra_servers, is_initialized=True
    ):
        self._bucket_size = bucket_size
        self._s2cell_level = s2cell_level
        self._all_s2cells = _compute_all_s2cells(s2cell_level)

        execution_profile = cassandra.cluster.ExecutionProfile(
            # High request timeout for the consumer-db-importer's big batch
            # requests.
            # TODO: make this configurable and set it only for the processes
            # that need it.
            request_timeout=20.0,
            load_balancing_policy=cassandra.policies.RoundRobinPolicy(),
        )

        self._cluster = cassandra.cluster.Cluster(
            contact_points=cassandra_servers,
            reconnection_policy=cassandra.policies.ConstantReconnectionPolicy(
                delay=1, max_attempts=None
            ),
            execution_profiles={
                cassandra.cluster.EXEC_PROFILE_DEFAULT: execution_profile
            },
        )

        self._region_coverer = s2sphere.RegionCoverer()
        self._region_coverer.min_level = s2cell_level
        self._region_coverer.max_level = s2cell_level

    def connect(self, is_initialized=True):
        self._session = self._cluster.connect()

        if not is_initialized:
            return

        # the following assume the DB has its tables created.
        self._session.set_keyspace("icarus")
        self._prepped_insert = self._session.prepare(
            """
            INSERT INTO data (
            bucket,
            s2cell,
            ts,
            lat,
            lon,
            icao,
            callsign,
            velocity,
            heading,
            baro_altitude,
            geo_altitude,
            vertical_rate,
            squawk
            ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
            """
        )

        self._prepped_data_select = self._session.prepare(
            """
            SELECT * FROM data
            WHERE
            bucket=? AND s2cell IN ?
            """
        )

        self._prepped_data_icao_select = self._session.prepare(
            """
            SELECT * FROM data
            WHERE
            bucket=? AND s2cell IN ? AND icao=?
            """
        )

        self._prepped_get_flight_by_icao = self._session.prepare(
            """
            SELECT
            id,
            callsign,
            first_seen_at,
            last_seen_at
            FROM flights_by_icao WHERE
            icao=?
            ORDER BY first_seen_at DESC
            LIMIT 1
            """
        )
        self._prepped_get_active = self._session.prepare(
            """
            SELECT * FROM active_flights
            WHERE zero=0
            """
        )

        # TODO: this will be executed as an update when it already exists.
        self._prepped_set_active = self._session.prepare(
            """
            INSERT INTO active_flights (
                zero,
                icao,
                flight_id,
                first_seen_at,
                last_seen_at,
                callsign
            ) VALUES (0, ?, ?, ?, ?, ?)
            """
        )

        self._prepped_delete_active = self._session.prepare(
            """
            DELETE FROM active_flights WHERE
            zero=0 AND icao=?
            """
        )

        self._prepped_insert_flight = self._session.prepare(
            """
            INSERT INTO flights (
                id,
                icao,
                callsign,
                first_seen_at
            ) VALUES (?, ?, ?, ?)
            """
        )

        self._prepped_end_flight = self._session.prepare(
            """
            UPDATE flights SET
            last_seen_at=?
            WHERE id=? AND first_seen_at=?
            """
        )

        self._prepped_get_flights_by = {}
        for param in {"icao", "callsign"}:
            self._prepped_get_flights_by[param] = self._session.prepare(
                f"""
                SELECT * FROM flights_by_{param}
                WHERE {param}=?
                """
            )

    def add_state(self, state: icarus.models.ObjectState, asynchronous=False):
        reported_at = state.reported_at
        bucket = timestamp_to_bucket(reported_at, self._bucket_size)
        s2cell = self.lat_lon_to_s2cell(state.lat, state.lon)

        state.compress()
        return self._execute(asynchronous)(
            self._prepped_insert,
            (
                # timestamps in cassandra are in ms.
                bucket * 1000,
                s2cell.id().id(),
                icarus.utils.unsigned_to_signed_32(state.reported_at),
                state.lat,
                state.lon,
                state.icao,
                self._unset_if_none(state.callsign),
                self._unset_if_none(
                    icarus.utils.unsigned_to_signed_16(state.velocity)
                    if state.velocity is not None
                    else None
                ),
                self._unset_if_none(
                    icarus.utils.unsigned_to_signed_16(state.heading)
                    if state.heading is not None
                    else None
                ),
                self._unset_if_none(
                    icarus.utils.unsigned_to_signed_16(state.baro_altitude)
                    if state.baro_altitude is not None
                    else None
                ),
                self._unset_if_none(
                    icarus.utils.unsigned_to_signed_16(state.geo_altitude)
                    if state.geo_altitude is not None
                    else None
                ),
                self._unset_if_none(state.vertical_rate),
                self._unset_if_none(state.squawk),
            ),
        )

    def get_states(self, timestamp, bbox):
        bbox = s2sphere.LatLngRect(
            s2sphere.LatLng.from_degrees(bbox[0], bbox[1]),
            s2sphere.LatLng.from_degrees(bbox[2], bbox[3]),
        )
        s2cells = self._region_coverer.get_covering(bbox)
        s2cells = [cell.id() for cell in s2cells]

        # select the previous bucket too, because at the time of the bucket
        # switch, we might still be filling up data.
        bucket = timestamp_to_bucket(timestamp, self._bucket_size)
        prev_bucket = timestamp_to_bucket(bucket - 1, self._bucket_size)
        s2cells = cassandra.query.ValueSequence(s2cells)

        with icarus.utils.measure_time("get_states query execution"):
            queries = []
            prev_bucket_q = self._session.execute(
                self._prepped_data_select,
                (
                    # timestamps in cassandra are in ms.
                    prev_bucket * 1000,
                    s2cells,
                ),
            )

            queries.append(prev_bucket_q)
            curr_bucket_q = self._session.execute(
                self._prepped_data_select,
                (
                    # timestamps in cassandra are in ms.
                    bucket * 1000,
                    s2cells,
                ),
            )

            queries.append(curr_bucket_q)

        # need to reconstruct from diff rows.
        # TODO: possibly overwriting newer states with older, because it orders
        # by s2cell first, and then ts.
        states = {}

        with icarus.utils.measure_time("get_states query consumption"):
            num_rows = 0
            for row in itertools.chain(prev_bucket_q, curr_bucket_q):
                num_rows += 1
                state = self.row_to_own_model(row)
                icao = state.icao
                if icao not in states:
                    states[icao] = state
                else:
                    for k, v in state.items():
                        if v is not None:
                            setattr(states[icao], k, v)

            log.debug("get_states got %s rows", num_rows)

        with icarus.utils.measure_time("get_states application level bbox"):
            for k in list(states.keys()):
                state = states[k]
                if not bbox.contains(
                    s2sphere.LatLng.from_degrees(state.lat, state.lon)
                ):
                    # application-level final bbox.
                    del states[k]

        return states.values()

    def add_flight(self, _id, state, asynchronous=False):
        return self._execute(asynchronous)(
            self._prepped_insert_flight,
            (
                _id,
                state.icao,
                self._unset_if_none(state.callsign),
                state.reported_at * 1000,
            ),
        )

    def end_flight(self, flight_id, last_seen_at, first_seen_at, asynchronous=False):
        return self._execute(asynchronous)(
            self._prepped_end_flight,
            (last_seen_at * 1000, flight_id, first_seen_at * 1000),
        )

    def get_last_flight_by_icao(self, icao):
        result_set = self._session.execute(self._prepped_get_flight_by_icao, (icao,))

        row = next(iter(result_set))
        last_seen_at = row.last_seen_at.timestamp() if row.last_seen_at else None
        flight = icarus.models.Flight(
            _id=row.id,
            first_seen_at=row.first_seen_at.timestamp(),
            last_seen_at=last_seen_at,
            icao=icao,
            callsign=row.callsign,
        )

        curr_bucket = timestamp_to_bucket(flight.first_seen_at, self._bucket_size)

        end = last_seen_at or time.time()
        futures = []
        while curr_bucket < end:
            fut = self._session.execute_async(
                self._prepped_data_icao_select,
                (curr_bucket * 1000, self._all_s2cells, icao),
            )

            futures.append(fut)
            curr_bucket = curr_bucket + self._bucket_size

        states = []
        log.debug(f"needed {len(futures)} queries to get the flight by icao")
        for future in futures:
            rows = future.result()
            for row in rows:
                # TODO: no deduplicator, but we've ditched that anyway?
                states.append(self.row_to_own_model(row))

        return states

    def get_flights_by_icao(self, icao, asynchronous=False):
        return self._execute(asynchronous)(
            self._prepped_get_flights_by["icao"], (icao,)
        )

    def get_flights_by_callsign(self, callsign, asynchronous=False):
        return self._execute(asynchronous)(
            self._prepped_get_flights_by["callsign"], (callsign,)
        )

    def get_active_flights(self):
        return {
            row.icao: icarus.models.ActiveFlight(
                icao=row.icao,
                flight_id=row.flight_id,
                first_seen_at=row.first_seen_at.timestamp(),
                last_seen_at=row.last_seen_at.timestamp(),
                callsign=row.callsign,
            )
            for row in self._session.execute(self._prepped_get_active)
        }

    def set_active_flight(
        self, active_flight: icarus.models.ActiveFlight, asynchronous=False
    ):
        return self._execute(asynchronous)(
            self._prepped_set_active,
            (
                active_flight.icao,
                active_flight.flight_id,
                active_flight.first_seen_at * 1000,
                active_flight.last_seen_at * 1000,
                active_flight.callsign,
            ),
        )

    def delete_active_flight(self, icao, asynchronous=True):
        return self._execute(asynchronous)(self._prepped_delete_active, (icao,))

    def _execute(self, asynchronous):
        return self._session.execute_async if asynchronous else self._session.execute

    @staticmethod
    def _unset_if_none(val):
        return val or cassandra.query.UNSET_VALUE

    @staticmethod
    def row_to_own_model(row):
        def apply_if_not_none(fn, value):
            if value is not None:
                return fn(value)

        if row.heading is not None:
            heading = icarus.utils.signed_to_unsigned_16(row.heading) / 100
        else:
            heading = None

        # Essentially drop bucket.
        # TODO: this is error prone when fields are edited.
        return icarus.models.ObjectState(
            icao=row.icao,
            callsign=row.callsign,
            lat=row.lat / 10000000,
            lon=row.lon / 10000000,
            geo_altitude=apply_if_not_none(
                icarus.utils.signed_to_unsigned_16, row.geo_altitude
            ),
            baro_altitude=apply_if_not_none(
                icarus.utils.signed_to_unsigned_16, row.baro_altitude
            ),
            velocity=apply_if_not_none(
                icarus.utils.signed_to_unsigned_16, row.velocity
            ),
            heading=heading,
            vertical_rate=row.vertical_rate,
            squawk=row.squawk,
            reported_at=icarus.utils.signed_to_unsigned_32(row.ts),
        )

    def lat_lon_to_s2cell(self, lat, lon):
        lat_lng = s2sphere.LatLng.from_degrees(lat, lon)

        # this is the level 30 cell
        s2cell = s2sphere.Cell.from_lat_lng(lat_lng)

        # level 3 is:
        tmp = s2cell.id()
        for _ in range(30 - self._s2cell_level):
            tmp = tmp.parent()

        s2cell = s2sphere.Cell(tmp)
        assert s2cell.level() == self._s2cell_level
        return s2cell
