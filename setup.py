#!/usr/bin/env python

from distutils.core import setup

setup(
    name="icarus",
    version="0.1",
    description="something",
    author="Dan Milon",
    author_email="i@***REMOVED***.me",
    packages=["icarus"],
)
