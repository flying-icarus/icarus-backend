import argparse

import confluent_kafka as kafka
import confluent_kafka.admin as kafka_admin
import cassandra
import cassandra.cluster
import cassandra.util

import icarus.utils
import icarus.db


def unset_if_none(val):
    return val or cassandra.query.UNSET_VALUE


parser = argparse.ArgumentParser()
parser.add_argument("--kafka-states-topic", required=True)
parser.add_argument("--kafka-server", required=True)
parser.add_argument('--bucket-size', required=True)
parser.add_argument('--s2cell-level', required=True, type=int)
parser.add_argument('--cassandra-server', required=True)

args = parser.parse_args()

db = icarus.db.DBClient(
    bucket_size=args.bucket_size,
    s2cell_level=args.s2cell_level,
    cassandra_servers=[args.cassandra_server],
)

db.connect(is_initialized=False)

db._session.execute("""
CREATE KEYSPACE IF NOT EXISTS icarus WITH
replication = {
  'class': 'SimpleStrategy',
  'replication_factor' : 0
};
""")
# bucket is 5-minute windows.
# TODO: pick a better primary key to split a bucket evenly, but still not
# query too many partitions.
data_trail_columns = """
    ts int,
    lat int,
    lon int,
    icao int,
    callsign text,
    velocity smallint,
    heading smallint,
    baro_altitude smallint,
    geo_altitude smallint,
    vertical_rate smallint,
    squawk smallint
"""

# TODO: prefix or suffix partition key by an int to scale to the number of
# nodes. Or add a high-level s2cell number? But that adds locality to the
# host spots.
# TODO: this compaction is a brain fart.
db._session.execute(f"""
CREATE TABLE IF NOT EXISTS icarus.data (
    bucket timestamp,
    s2cell varint,
    {data_trail_columns},
    PRIMARY KEY ((bucket), s2cell, icao, ts))
WITH
compaction = {{
  'class': 'SizeTieredCompactionStrategy'
}} AND
compression = {{
  'sstable_compression': 'org.apache.cassandra.io.compress.LZ4Compressor',
  'chunk_length_kb': 512
}}
""")

db._session.execute("""
CREATE TABLE IF NOT EXISTS icarus.active_flights (
    zero int,
    icao int,
    first_seen_at timestamp,
    last_seen_at timestamp,
    flight_id uuid,
    callsign text,
    PRIMARY KEY (zero, icao)
)
""")

# TODO: join together ID and first_seen_at in a UUID!!
db._session.execute(f"""
CREATE TABLE IF NOT EXISTS icarus.flights (
    id uuid,
    first_seen_at timestamp,
    last_seen_at timestamp,
    icao int,
    callsign text,
    PRIMARY KEY (id, first_seen_at)
)
WITH CLUSTERING ORDER BY (first_seen_at DESC)
""")

db._session.execute("""
CREATE MATERIALIZED VIEW IF NOT EXISTS icarus.flights_by_icao AS
SELECT * FROM flights
WHERE icao IS NOT NULL AND first_seen_at IS NOT NULL
PRIMARY KEY (icao, first_seen_at, id)
WITH CLUSTERING ORDER BY (first_seen_at DESC)
""")

db._session.execute("""
CREATE MATERIALIZED VIEW IF NOT EXISTS icarus.flights_by_callsign AS
SELECT * FROM flights
WHERE callsign IS NOT NULL AND first_seen_at IS NOT NULL
PRIMARY KEY (callsign, first_seen_at, id)
WITH CLUSTERING ORDER BY (first_seen_at DESC)
""")

kafka_client = kafka_admin.AdminClient({
    'bootstrap.servers': args.kafka_server,
    'group.id': 'mygroup',
    'auto.offset.reset': 'earliest'
})

# TODO: increase number of partitions for production.
futures = kafka_client.create_topics([
    kafka_admin.NewTopic(
        topic=args.kafka_states_topic,
        num_partitions=1,
        replication_factor=1,
        config={
            # TODO: switch to zstd when support lands in librdkafka.
            'compression.type': 'snappy',
        },
    ),
])

for fut in futures.values():
    try:
        fut.result()
    except kafka.KafkaException as exc:
        if exc.args[0].code() != kafka.KafkaError.TOPIC_ALREADY_EXISTS:
            raise
